import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculateGradeTest {

    @Test
    public void testGradeA() {
        assertEquals("A", CalculateGrade.calculate(88));
    }

    @Test
    public void testGradeB() {
        assertEquals("B", CalculateGrade.calculate(78));
    }

    @Test
    public void testGradeC() {
        assertEquals("C", CalculateGrade.calculate(68));
    }


    @Test
    public void testGradeD() {
        assertEquals("D", CalculateGrade.calculate(58));
    }

    @Test
    public void testGradeF() {
        assertEquals("F", CalculateGrade.calculate(48));
    }


}
